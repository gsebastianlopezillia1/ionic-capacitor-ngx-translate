import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FolderPageRoutingModule } from './folder-routing.module';

import { FolderPage } from './folder.page';
import { HttpClientModule, HttpClient } from "@angular/common/http";//TO USE TRANSLATE
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';//TO USE TRANSLATE
import { HttpLoaderFactory } from '../app.module';//TO USE TRANSLATE

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FolderPageRoutingModule,
    HttpClientModule,//TO USE TRANSLATE
    TranslateModule.forChild(),//TO USE TRANSLATE
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })//TO USE TRANSLATE
  ],
  declarations: [FolderPage]
})
export class FolderPageModule {}
