import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";//TO USE TRANSLATE
//REMEMBER THE IMPORT IN THE COMPONENT MODULE, IT'S REQUIRED TO LAZY LOADING
@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  lang: boolean = true;
  public folder: string;

  constructor(private activatedRoute: ActivatedRoute,
    private translate: TranslateService) { }//TO USE TRANSLATE

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }

  updateLang(){//SET THE TRANSLATE ROOT FILE: ...src/app/assets/i18n/langX.json
    if(this.lang){
      this.translate.use('es')
    }else{
      this.translate.use('en')
    }
  }

}
